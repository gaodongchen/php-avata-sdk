<?php
namespace avata;

class Response {
    
    /**
     * Avata平台API响应结果
     *
     * @var string
     */
    private string $_response;

    function __construct(string $response)
    {
        $this->_response = $response;
    }

    public function getResponse(): string
    {
        return $this->_response;
    }

    public function getResponseArray(): array
    {
        if(!empty($this->_response)){
            return json_decode($this->_response, true);
        }
        return [];
    }

    public function getData(): array
    {
        return !empty($this->getResponseArray()) ? $this->getResponseArray()['data'] : [];
    }
}
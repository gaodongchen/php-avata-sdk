<?php

namespace avata\query;

use avata\Query;

class BatchMint extends Query
{

    /**
     * 平台资源路径
     *
     * @var string
     */
    protected string $path = '/v1beta1/nft/batch/nfts/';

    /**
     * 请求方式
     *
     * GET | POST
     *
     * @var string
     */
    protected string $method = 'POST';

    function __construct(array $body = [], string $classId)
    {
        $this->path = $this->path . $classId;

        if (!isset($body['operation_id']))
            $body['operation_id'] = 'operationid' . $this->time() . mt_rand(10000000000000000, 99999999999999999);

        parent::__construct([], $body);
    }
}
<?php

namespace avata\query;

use avata\Query;

class Transfers extends Query
{

    /**
     * 平台资源路径
     *
     * @var string
     */
    protected string $path = '/v1beta1/nft/nft-transfers/';

    /**
     * 请求方式
     *
     * GET | POST
     *
     * @var string
     */
    protected string $method = 'POST';

    /**
     * NFT转让
     *
     * @param string $class_id
     * @param string $nft_id
     * @param string $from_account
     * @param string $to_account
     */
    function __construct(string $class_id, string $nft_id, string $from_account, string $to_account)
    {
        $this->path = $this->path . $class_id . '/' . $from_account . '/' . $nft_id;

        parent::__construct([], [
            'recipient' => $to_account
        ]);
    }
}
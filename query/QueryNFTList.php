<?php

namespace avata\query;

use avata\Query;

class QueryNFTList extends Query
{

    /**
     * 平台资源路径
     *
     * @var string
     */
    protected string $path = '/v1beta1/nft/nfts';

    /**
     * 请求方式
     *
     * GET | POST
     *
     * @var string
     */
    protected string $method = 'GET';

    function __construct(array $query)
    {
        parent::__construct($query);
    }
}
<?php
namespace avata\query;

use avata\Query;

class CreateAccount extends Query {
    
    /**
     * 平台资源路径
     *
     * @var string
     */
    protected string $path = '/v1beta1/account';

    /**
     * 请求方式
     * 
     * GET | POST
     *
     * @var string
     */
    protected string $method = 'POST';

    function __construct(array $body=[])
    {        
        parent::__construct([], $body);
    }
}
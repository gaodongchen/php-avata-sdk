<?php
namespace avata\query;

use avata\Query;

class AccountSearch extends Query {
    
    /**
     * 平台资源路径
     *
     * @var string
     */
    protected string $path = '/v1beta1/accounts';

    /**
     * 请求方式
     * 
     * GET | POST
     *
     * @var string
     */
    protected string $method = 'GET';

    function __construct(array $query=[])
    {
        if(!isset($query['operation_id']))
            $query['operation_id'] = 'operationid' . $this->time();
        
        parent::__construct($query, []);
    }
}
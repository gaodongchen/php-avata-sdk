<?php

namespace avata\query;

use avata\Query;

class CreateClass extends Query
{

    /**
     * 平台资源路径
     *
     * @var string
     */
    protected string $path = '/v1beta1/nft/classes';

    /**
     * 请求方式
     *
     * GET | POST
     *
     * @var string
     */
    protected string $method = 'POST';

    function __construct(array $body = [])
    {
        parent::__construct([], $body);
    }

    /**
     * 名称
     *
     * @param string $name
     * @return CreateClass
     */
    public function setName(string $name): CreateClass
    {
        $this->body['name'] = $name;
        return $this;
    }

    /**
     * NFT 类别权属者地址，支持任一文昌链合法链账户地址
     *
     * @param string $account
     * @return CreateClass
     */
    public function setOwner(string $account): CreateClass
    {
        $this->body['owner'] = $account;
        return $this;
    }
}
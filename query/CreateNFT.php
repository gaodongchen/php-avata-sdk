<?php
namespace avata\query;

use avata\Query;

/**
 * 发行NFT
 * 
 * @package Query
 * @author 肥臣 <gaodongchen@qq.com>
 */
class CreateNFT extends Query {
    
    /**
     * 平台资源路径
     *
     * @var string
     */
    protected string $path = '/v1beta1/nft/nfts/';

    /**
     * 请求方式
     * 
     * GET | POST
     *
     * @var string
     */
    protected string $method = 'POST';

    /**
     * 构造
     *
     * @param string $class_id
     * @param string $name
     * @param array $body
     */
    function __construct(string $class_id, string $name, array $body=[])
    {
        $this->path = $this->path . $class_id;
        $body['name'] = $name;
        parent::__construct([], $body);
    }

    /**
     * NFT 接收者地址，支持任一文昌链合法链账户地址
     * 
     * 非必须，默认为 NFT 类别的权属者地址
     *
     * @param string $account
     * @return CreateNFT
     */
    function setRecipient(string $account): CreateNFT
    {
        $this->body['recipient'] = $account;

        return $this;
    }
}
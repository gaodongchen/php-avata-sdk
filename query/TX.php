<?php

namespace avata\query;

use avata\Query;

class TX extends Query
{

    /**
     * 平台资源路径
     *
     * @var string
     */
    protected string $path = '/v1beta1/tx/';

    /**
     * 请求方式
     *
     * GET | POST
     *
     * @var string
     */
    protected string $method = 'GET';

    function __construct(string $operation_id)
    {
        $this->path = $this->path . $operation_id;
        
        parent::__construct();
    }
}